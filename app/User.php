<?php

namespace App;

use App\Models\Profession;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = 'users'; podemos especificar el nombre de la tabla que hace referencia el modelo
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $cast = [
        'is_admin' => 'boolean'
    ];

    public function profession()
    {
        return $this->belongsTo(Profession::class, 'profession_id');
    }

    public function isAdmin(){
        return $this->id_admin;
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
