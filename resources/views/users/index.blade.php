@extends('layout')

@section('title', "Usuarios")
@section('content')
    <div class="d-flex justify-content-between" style="margin-bottom:40px; margin-top: 20px;">
        <h1>{{ $title }}</h1>
        <p>
            <a href="{{ route('users.create') }}" class="btn btn-primary">Nuevo usuario</a>
        </p>
    </div>
   
    @if($users)
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <form action="{{ route('users.destroy', $user) }}" method="POST">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <a  class="btn btn-link"  href="{{ route('users.show', $user) }}"><span class="oi oi-eye"></span></a>
                                <a class="btn btn-link"  href="{{ route('users.edit', ['user' => $user]) }}"><span class="oi oi-pencil"></span></a>
                                <button type="submit" class="btn btn-link"><span class="oi oi-trash"></span></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
    @else
        <p>No hay usuarios registrados</p>
    @endif
@endsection
