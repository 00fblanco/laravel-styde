@extends('layout')

@section('title', "Crear usuario")

@section('content')
    <h1>Editar usuario</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <h6>Por favor corrige los errores debajo:</h6>
            {{--  <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>  --}}
        </div>
        
    @endif
    <form action="{{ url("usuarios/{$user->id}")}}" method="POST">
        {{ method_field('PUT') }}
        {!! csrf_field() !!}
        <label for="name">Nombre</label>
        <input type="text" name="name" placeholder="Pedro perez" value="{{ old('name', $user->name) }}">
        @if($errors->has('name'))
            <p style="font-size: 12px; color: red;margin: 0;">{{$errors->first('name')}}</p>
        @endif
        <br>
        <label for="email">Correo electrónico</label>
        <input type="email" name="email" placeholder="pedro@fma.com" value="{{ old('email', $user->email) }}">
        @if($errors->has('email'))
            <p style="font-size: 12px; color: red; margin: 0">{{$errors->first('email')}}</p>
        @endif
        <br>
        <label for="password">Contraseña</label>
        <input type="password" name="password" placeholder="Mayor a 6 caracteres" >
        @if($errors->has('password'))
            <p style="font-size: 12px; color: red;margin:0">{{$errors->first('password')}}</p>
        @endif
        <br>
        <button type="submit">Actualizar usuario</button>
    </form>
    <p>
        {{--  <a href="{{ url('/usuarios')}}">Regresar</a>  --}}
        {{--  <a href="{{ action('UserController@index') }}">Regresar</a>  --}}
        <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>
    </p>
@endsection
