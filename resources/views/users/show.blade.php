@extends('layout')

@section('title', "Usuario {$user->id}")

@section('content')
    <h1>Usuario #{{ $user->id }}</h1>
    <p>Mostrando detalle del usuario: {{ $user->id }}</p>
    <p>Correo electronico: {{ $user->email }}</p>

    <p>
        {{--  <a href="{{ url('/usuarios')}}">Regresar</a>  --}}
        {{--  <a href="{{ action('UserController@index') }}">Regresar</a>  --}}
        <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>
    </p>
@endsection
