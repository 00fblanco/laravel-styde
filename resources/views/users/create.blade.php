@extends('layout')

@section('title', "Crear usuario")

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Crear nuevo usuario</h4>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <h6>Por favor corrige los errores debajo:</h6>
                    {{--  <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>  --}}
                </div>
                
            @endif
            <form action="{{ url('usuarios')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input class="form-control" type="text" name="name" placeholder="Pedro perez" value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <p style="font-size: 12px; color: red; margin: 0">{{$errors->first('name')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input class="form-control" type="email" name="email" placeholder="pedro@fma.com" value="{{ old('email') }}">
                        @if($errors->has('email'))
                            <p style="font-size: 12px; color: red; margin: 0">{{$errors->first('email')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input class="form-control" type="password" name="password" placeholder="Mayor a 6 caracteres" >
                        @if($errors->has('password'))
                            <p style="font-size: 12px; color: red;margin:0">{{$errors->first('password')}}</p>
                        @endif
                    </div>
                    <button type="submit">Crear usuario</button>
                </form>
                <p>
                    {{--  <a href="{{ url('/usuarios')}}">Regresar</a>  --}}
                    {{--  <a href="{{ action('UserController@index') }}">Regresar</a>  --}}
                    <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>
                </p>
        </div>
    </div>
@endsection
