<?php

use App\User;
use App\Models\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $professions = DB::select('select id from professions where title = ?', ["Desarrollador back-end"]);
        // $professionId = DB::table('professions')
        //     ->whereTitle('Desarrollador back-end')
        //     ->value('id');
        $professionId = Profession::where('title', 'Desarrollador back-end')->value('id');

        User::create([
            'name' => 'Francisco Blanco',
            'email' => "00fblanco@gmail.com",
            'password' => bcrypt('laravel') ,
            'profession_id' => $professionId,
            'is_admin' => true
        ]);

        factory(User::class)->create([
            'profession_id' => $professionId
        ]);
        factory(User::class, 10)->create();
    }
}
