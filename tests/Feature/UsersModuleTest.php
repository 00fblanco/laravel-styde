<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersModuleTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    function it_load_the_users_list_page()
    {
        factory(User::class)->create([
            'name' => 'Joel',
        ]);
        factory(User::class)->create([
            'name' => 'Elli'
        ]);

        $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee('Listado de usuarios')
            ->assertSee('Joel')
            ->assertSee('Elli');
    }
     /** @test */
     function it_shows_a_default_message_if_the_users_list_is_empty()
     {
         $this->get('/usuarios')
            ->assertStatus(200)
            ->assertSee('No hay usuarios registrados');
     }

    /** @test */
    function it_displays_the_users_details()
    {
        $user = factory(User::class)->create([
            'name' => 'Frank'
        ]);

        $this->get("/usuarios/{$user->id}")
            -> assertStatus(200)
            ->assertSee("Mostrando detalle del usuario: {$user->id}");
    }

    /** @test */
    function it_load_the_new_users_page()
    {
        $this->get('/usuarios/nuevo')
            -> assertStatus(200)
            ->assertSee('Crear nuevo usuario');
    }

    /** @test */
    function it_displays_a_404_error_if_the_user_is_not_found()
    {
        $this->get('/usuarios/999')
            -> assertStatus(404)
            ->assertSee('Página no encontrada');
    }

    /** @test */
    function it_creates_a_new_user()
    {
        $this->post('/usuarios/', [
            'name' => 'Duilio',
            'email' => 'dulio@gmail.com',
            'password' => '123456'
        ])->assertRedirect('usuarios/');

        $this->assertCredentials([ // para comprobar si se guardo con la contraseña correcta
            'name' => 'Duilio',
            'email' => 'dulio@gmail.com',
            'password' => '123456'
        ]);
    }
    /** @test */
    function the_name_is_required()
    {
        $this->from('usuarios/nuevo')->post('/usuarios/', [
            'name' => '',
            'email' => 'duli@gmail.com',
            'password' => '123456'
        ])->assertRedirect('usuarios/nuevo')
            ->assertSessionHasErrors(['name' => 'El campo nombre es obligatorio']);

        $this->assertEquals(0, User::count());
            // assertDatabaseMissing('users', [
        //     'email' => 'duilio@styde.net'
        // ]);
    }
    /** @test */
    function the_mail_is_required()
    {
        $this->from('usuarios/nuevo')->post('/usuarios/', [
            'name' => 'Pedro',
            'email' => '',
            'password' => '123456'
        ])->assertRedirect('usuarios/nuevo')
            ->assertSessionHasErrors(['email' => 'El campo email es obligatorio']);

        $this->assertEquals(0, User::count());
    }
    /** @test */
    function the_mail_must_be_valid()
    {
        $this->from('usuarios/nuevo')->post('/usuarios/', [
            'name' => 'Pedro',
            'email' => 'correo-no-valido',
            'password' => '123456'
        ])->assertRedirect('usuarios/nuevo')
            ->assertSessionHasErrors(['email']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function the_mail_must_be_unique()
    {
        factory(User::class)->create([
            'email' => 'pedro@gmail.com'
        ]);

        $this->from('usuarios/nuevo')->post('/usuarios/', [
            'name' => 'Pedro',
            'email' => 'pedro@gmail.com',
            'password' => '123456'
        ])->assertRedirect('usuarios/nuevo')
            ->assertSessionHasErrors(['email']);

        $this->assertEquals(1, User::count());
    }

    /** @test */
    function the_password_is_required()
    {
        $this->from('usuarios/nuevo')->post('/usuarios/', [
            'name' => 'Pedro',
            'email' => 'pedro@gmail.com',
            'password' => ''
        ])->assertRedirect('usuarios/nuevo')
            ->assertSessionHasErrors(['password' => 'El campo password es obligatorio']);

        $this->assertEquals(0, User::count());
    }

     /** @test */
     function it_load_the_edit_user_page()
     {
         $user = factory(User::class)->create();

         $this->get("/usuarios/{$user->id}/editar/")
             -> assertStatus(200)
             ->assertViewIs('users.edit')
             ->assertSee('Editar usuario')
             ->assertViewHas('user', function($viewUser) use($user){
                 return $viewUser->id == $user->id;
             });
     }

     /** @test */
     function it_update_a_user()
     {
         $this->withoutExceptionHandling();
         $user = factory(User::class)->create();
         $this->put("/usuarios/{$user->id}", [
            'name' => 'edit Duilio',
            'email' => 'dulio@new.com',
            'password' => '123456'
        ])->assertRedirect("usuarios/{$user->id}");
        
        $this->assertCredentials([ // para comprobar si se guardo con la contraseña correcta
            'name' => 'edit Duilio',
            'email' => 'dulio@new.com',
            'password' => '123456'
        ]);
     }

     /** @test */
     function the_name_is_required_when_updating_a_user()
     {
         $user = factory(User::class)->create();
         $this->from("usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => '',
            'email' => 'dulio@new.com',
            'password' => '123456'
        ])->assertRedirect("usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['name']);
        
        $this->assertDatabaseMissing('users', ['email' => 'dulio@new.com']);
     }
     /** @test */
     function the_email_must_be_valid_when_updating_a_user()
     {
         $user = factory(User::class)->create();
         $this->from("usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Frank',
            'email' => 'novalidnew.com',
            'password' => '123456'
        ])->assertRedirect("usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['email']);
        
        $this->assertDatabaseMissing('users', ['name' => 'Frank']);
     }
    /** @test */
     function the_email_must_be_unique_when_updating_a_user()
     {
        $randomUser = factory(User::class)->create([
            'email' => 'existing@gmail.com'
        ]);

         $user = factory(User::class)->create([
             'email' => 'dulio@gmail.com'
         ]);
         
         $this->from("usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Frank',
            'email' => 'existing@gmail.com',
            'password' => '123456'
        ])->assertRedirect("usuarios/{$user->id}/editar")
        ->assertSessionHasErrors(['email']);
        
        // $this->assertDatabaseMissing('users', ['name' => 'Frank']);
     }
     /** @test */
     function the_password_is_optional_when_updating_a_user()
     {
         $user = factory(User::class)->create([
             'password' => bcrypt('clave_anterior')
         ]);
         
         $this->from("usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Frank',
            'email' => 'dulio@gmail.com',
            'password' => ''
        ])->assertRedirect("usuarios/{$user->id}"); //user show
        
        $this->assertCredentials([
            'email' => 'dulio@gmail.com',
            'password' => 'clave_anterior'
        ]);
     }

     /** @test */
     function the_users_email_can_stay_the_same_when_updating_the_user()
     {
         $user = factory(User::class)->create([
             'email' => bcrypt('fblanco@gmail.com')
         ]);
         
         $this->from("usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            'name' => 'Frank',
            'email' => 'fblanco@gmail.com',
            'password' => '12345678'
        ])->assertRedirect("usuarios/{$user->id}"); //user show
        
        $this->assertDatabaseHas('users', [
            'name' => 'Frank',
            'email' => 'fblanco@gmail.com',
        ]);
     }
     /** @test */
     function it_deletes_a_user()
     {
         $user = factory(User::class)->create();
         
         $this->delete("/usuarios/{$user->id}")
            ->assertRedirect("usuarios"); //user list
        
        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);
     }
     
}
